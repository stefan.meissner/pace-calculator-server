package org.sme_sports.pace_calculator.server;

import org.sme_sports.pace_calculator.server.dto.Version;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VersionController {

	@RequestMapping("/version")
    public Version version() {
        return new Version();
    }
}
